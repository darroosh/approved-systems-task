<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends \Eloquent
{
    protected $table = "company";    
    protected $fillable = [
        'name',
    ];
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    public function employees()
    {
        
        return $this->  hasMany( 'App\Employees' , 'company_id') ;
        
    }
    
}
