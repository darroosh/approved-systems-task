<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Image;
use Datatables;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        
        return view('companies.index', compact('companies'));
    }
    /**
     * returns json datatables required data.
     */
    
    public function dataTables()
    {
        $model = Company::with('image');
//         return dataTables()->of(Company::query())->make(true);
        return dataTables()->eloquent($model)
        ->addColumn('image', function (Company $company) {
            if(is_object($company->image)){
                return $company->image->image;
            }
            return null;
        })
        ->toJson();
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'=>    ['required',
                'unique:company',
            ],
            'image'=>'image',
        ]);
        
        $company = new Company([
            'name' => $request->get('name'),
        ]);
        $company->save();
        
        //Save image
        if($request->has('image')){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('uploads/companies'), $imageName);
            $image = new Image([
                'image' =>  $imageName,
            ]);
            $company->image()->save($image);
        }
        return redirect('/companies')->with('success', 'Company saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
