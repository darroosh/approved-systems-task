<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Employee;
use App\Company;
use App\Image;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employees = Employee::all();
        
        return view('employees.index', compact('employees'));
    }
    /**
     * returns json datatables required data.
     */
    
    public function dataTables()
    {
        $model = Employee::with(['image','company']);
        //         return dataTables()->of(Company::query())->make(true);
        return dataTables()->eloquent($model)
        ->addColumn('image', function (Employee $emp) {
            if(is_object($emp->image)){
                return $emp->image->image;
            }
            return null;
        })
        ->addColumn('company', function (Employee $emp) {
            return $emp->company->name;
        })
        ->toJson();
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::get(['id','name'])->pluck('name','id');        
        return view('employees.create')->with('companies',$companies);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name'=>    ['required',
                           'unique:employee',
            ],
            'image'=>'image',
            'company_id'=>'required|numeric'
        ]);
        
        $employee = new Employee([
            'name' => $request->get('name'),
            'company_id' => $request->get('company_id'),
        ]);
        $employee->save();
        
        //Save image
        if($request->has('image')){
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('uploads/employees'), $imageName);
            $image = new Image([
                'image' =>  $imageName,
            ]);
            $employee->image()->save($image);
            
            
        }
        return redirect('/employees')->with('success', 'Employee saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
