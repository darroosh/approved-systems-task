<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends \Eloquent
{
    protected $table = "employee";
    protected $fillable = [
        'name',
        'company_id',
    ];
    //
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }
}
