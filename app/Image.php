<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends \Eloquent
{
        protected $table = "Images";
        protected $fillable = [
            'image',
        ];
        
    public function imageable()
    {
        return $this->morphTo();
    }
    
    
    public function getImageAttribute($val){
        if($this->imageable_type == 'App\Company'){
            return url( 'uploads/companies/' . $val);
        }elseif($this->imageable_type == 'App\Employee'){
            return url( 'uploads/employees/' . $val);
        }
    }
}
