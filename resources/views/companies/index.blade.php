@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
    		<a class='btn btn-success float-right' href='{{ url("companies/create") }}'>Add new company</a>
            <h1 class="display-3">Companies</h1>
          <table class="table table-striped" id="table">
            <thead>
                <tr>
                  <td>ID</td>
                  <td>Name</td>
                  <td>Image</td>
                </tr>
            </thead>
          </table>
          
          
          
          
          
        <div>
    </div>
@endsection


@section('scripts')
    <script>
         $(document).ready( function () {
        	    $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{{ url('companies/dataTables') }}',
                    columns: [
                             { data: 'id', name: 'id' },
                             { data: 'name', name: 'name' },
                             { data: 'image', 
                                 	  name: 'image',
                                 	  "render" : function ( data) {
                                 			if(data != null){
                                 				return '<img class="list-img" src="'+ data +'"/>';
                                 			}
                                 			return '';		                      	     	  
        			               	  } 
			               	  }
                          ],

                      
                 });
        	} );
         
     </script>

@endsection