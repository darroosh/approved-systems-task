@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-sm-12">
    		<a class='btn btn-success float-right' href='{{ url("employees/create") }}'>Add new employee</a>
            <h1 class="display-3">Employees</h1>    
          <table class="table table-striped" id="table">
            <thead>
                <tr>
                  <td>ID</td>
                  <td>Name</td>
                  <td>Company</td>
                  <td>Image</td>
                </tr>
            </thead>
          </table>
          
          
          
          
          
        <div>
    </div>
@endsection


@section('scripts')
    <script>
         $(document).ready( function () {
        	    $('#table').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: '{{ url('employees/dataTables') }}',
                    columns: [
                             { data: 'id', name: 'id' },
                             { data: 'name', name: 'name' },
                             { data: 'company', name: 'company' },
                             { data: 'image', 
                                 	  name: 'image',
                                 	  "render" : function ( data) {
                                 			if(data != null){
                                 				return '<img class="list-img" src="'+ data +'"/>';
                                 			}
                                 			return '';		                      	     	  
        			               	  } 
			               	  }
                          ],

                      
                 });
        	} );
         
     </script>

@endsection