@extends('layouts.app')

@section('content')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add employee</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('employees.store') }}" enctype="multipart/form-data" >
          @csrf
          <div class="form-group">    
              <label for="first_name">Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="last_name">Image:</label>
              <input type="file" class="form-control" name="image" />
          </div>

          <div class="form-group">
              <label for="email">Company:</label>
              {!! Form::select('company_id', $companies) !!}	
              
          </div>
          <button type="submit" class="btn btn-success btn-primary-outline">Add Employee</button>
      </form>
  </div>
</div>
</div>
@endsection
