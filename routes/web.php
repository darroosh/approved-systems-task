<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes(['register' => false]);


Route::group(['middleware' => ['auth']], function () {
    Route::get('/', function () {
        return view('welcome');
    });
        
        
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/companies/dataTables', 'CompanyController@dataTables');
    Route::get('/employees/dataTables', 'EmployeeController@dataTables');
    
    Route::resources([
        'companies' => 'CompanyController',
        'employees' => 'EmployeeController'
    ]);
});
    

